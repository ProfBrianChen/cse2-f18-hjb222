import java.util.Scanner;

public class PatternA {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int nRows = 0;
    System.out.println("Please insert an integer between 0 and 10");
    nRows = scan.nextInt();
    while (nRows <= 0 || nRows > 10) {
      System.out.println("ERR: Please insert an integer between 0 and 10");
      nRows = scan.nextInt();      
    }
    
    for (int i=1; i<=nRows; i++) {
      int x = 1;
      for (int j = 0; j<nRows; j++) {
        while (x<=i) {
          System.out.print(x+" ");
          x++;
        }
      }
      
      System.out.println("");
    }
    
    
  }
}