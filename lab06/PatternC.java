import java.util.Scanner;

public class PatternC {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int nRows = 0;
    System.out.println("Please insert an integer between 0 and 10");
    nRows = scan.nextInt();
    while (nRows <= 0 || nRows > 10) {
      System.out.println("ERR: Please insert an integer between 0 and 10");
      nRows = scan.nextInt();      
    }
    
    for (int i=1; i<=nRows; i++) {
      int x = i;
      for (int n = 1; n<=(2*(nRows-i)); n++) {
        System.out.print(" ");
      }
      for (int j = 0; j<nRows; j++) {
        while (x>0) {
          System.out.print(x+" ");
          x--;
        }
      }
      
      System.out.println("");
    }
    
    
  }
}