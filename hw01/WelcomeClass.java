/**
* Welcome class prints out a welcome screen when run
*/
public class WelcomeClass {
  // main method
  public static void main(String[] args) {
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println(" ^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\ ");
    System.out.println("<-H--J--B--2--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println(" v  v  v  v  v  v");
  }
}