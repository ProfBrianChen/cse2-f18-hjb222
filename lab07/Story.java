import java.util.Random;
import java.util.Scanner;

public class Story {
  public static int rando(int n) {
    Random rand = new Random();
    return rand.nextInt(10);
  }
  public static String adjective() {
    String[] ads = {"broad", "assorted", "thinkable", "calm", "vast", "pastoral", "foregoing", "thin", "cloudy", "sneaky"};
    return ads[rando(10)];
  }
  public static String nounSub() {
    String[] nSubs = {"fox", "car", "cat", "dog", "plane", "train", "sister", "brother", "mother", "father"};
    return nSubs[rando(10)];
  }
  public static String verb() {
    String[] verbs = {"traded", "destroyed", "moved", "guided", "dragged", "delivered", "slapped", "killed", "advised", "discovered"};
    return verbs[rando(10)];
  }
  public static String nounObj() {
    String[] nounObjs = {"president", "plant", "bike", "worm", "cow", "creature", "zebra", "clam", "wheelbarrow", "teacher"};
    return nounObjs[rando(10)];
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    String sub = nounSub();
    int n = 1;
    while (n==1) {
      sub = nounSub();
      System.out.println("The " + adjective() +" "+ adjective() +" "+ sub +" "+ verb() +" "+ "the" +" "+ adjective() +" "+ nounObj()+".");
      System.out.println("Please insert '1' for another sentence");
      n = scan.nextInt();
    }
    
    System.out.println("This "+sub+" also "+verb()+" a large collection of "+nounObj()+"s.");
  }
}