import java.util.Scanner;

/**
* Convert prompts the user for an affected area (in acres) of rainfall, and the rainfall (in inches) for that area.
* It returns the total quantity of rain in cubic miles
*/
public class Convert {
  // main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    double acreToSquareMiles = 0.0015625; // conversion factor from acres to square miles
    double inchesToMiles = 0.0000157828; // conversion factor from inches to miles
    System.out.print("Enter the affected area in acres: ");
    double affectedArea = scan.nextDouble(); // this variable is in acres
    
    System.out.print("Enter the rainfall in the affected area: ");
    double rainfall = scan.nextDouble(); // this variable is in inches
    
    // conversions below
    affectedArea *= acreToSquareMiles;
    rainfall *= inchesToMiles;
    
    System.out.println((affectedArea*rainfall)+"  cubic miles");
    
  }// end main method
}// end class