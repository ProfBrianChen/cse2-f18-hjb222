import java.util.Scanner;

/** 
* Pyramid takes the square side length of the base, and the height of the pyramid, and prints the resulting volume
*/
public class Pyramid {
  // main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    double sideLength;
    double height;
    System.out.print("The square side of the pyramid is (input length): ");
    sideLength = scan.nextDouble();
    System.out.print("The height of the pyramid is (input height): ");
    height = scan.nextDouble();
    // calculation is the volume of a pyramid
    System.out.println("The volume inside the pyramid is: "+((sideLength*sideLength*height)/(3.0))); 
  }// end main method
}// end Pyramid