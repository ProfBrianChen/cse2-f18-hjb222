// Harry Boon
// 10/20/2018
import java.util.Scanner;

public class EncryptedX {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    
    
    System.out.println("Please input an integer between 0 and 100");
    int len = scan.nextInt(); // scanner should automatically produce an error if there is no integer input
    
    // validating that the integer they input is between 0 and 100
    while (len <  0 || len > 100) {
      System.out.println("Please input an integer between 0 and 100");
      len = scan.nextInt();
    }
    
    for (int i = 0; i < len; i++) {
      for (int j = 0; j < len; j++) {
        // by eliminating two stars based off of the row number (rownum, size-rownum-1), an X will be produced
        if (i==j || j==len-1-i) {
          System.out.print(" ");
        } else {
          System.out.print("*");
        }
      }
      // jump down to next row
      System.out.println("");
    }
    
  }
}