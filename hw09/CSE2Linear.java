import java.util.*;
// Harry Boon
// 
public class CSE2Linear {
  public static void sop(String s) {
    System.out.println(s);
  }
  public static int linSearch(int n, int[] a) {
    for (int i = 0; i < a.length; i++) {
      if (a[i]==n) {
        return i+1; // i+1 to show the iterations done
      }
    }
    return -1; // only runs if linSearch doesn't find the element it's looking for
  }
  
  public static int binSearch(int n, int[] a) {
    int left = 0;
    int right = a.length-1;
    int iterations = 0;
    while (right >= left) {
      iterations++;
      int half = (right+left)/2;
      if (a[half]==n) {
        return iterations; // found after this amount of iterations
      } else if(a[half] < n) {
        left = half + 1; // shift the bounds on the left side
      } else {
        right = half - 1; // only other option (shifting right bound)
      }
    }
    return -1; // never found
  }
  
  public static void scramble(int[] a) {
     Random r = new Random();
    for (int i=0; i < a.length; i++) {
      int rand = r.nextInt(a.length);
      int temp = a[rand];
      a[rand]=a[0];
      a[0]=temp;
    }
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    int[] grades = new int[15];
    int prev = -1;
    int g = 0;
    
    for (int i=0; i<grades.length; i++) {
      boolean isG = false;
      do {
        sop("Please insert int grade in ASCENDING order");
        isG = scan.hasNextInt();
        while (!isG) {
          scan.next();
          sop("ERR: Insert an integer");
          isG = scan.hasNextInt();
        }  
        g = scan.nextInt();
        if (g <= prev) {
          sop("ERR: Insert grades in ascending order");
        }
        if (g < 0 || g > 100) {
          sop("ERR: Keep it between 0-100");
          g=prev; // forces a restart in the loop
        }

    } while (g <= prev);
      prev = g;
      grades[i] = g;
    }
    for (int i=0; i<grades.length; i++) {
      System.out.print(grades[i]+", ");
    }
    sop("");
    sop("Please insert an integer to be searched for");
    int b = scan.nextInt();
    if (binSearch(b, grades) == -1) {
      sop("Not found");
    } else {
      sop("Found after "+binSearch(b, grades)+" iterations.");
    }
    sop("Scrambled");
    scramble(grades);
    for (int i=0; i<grades.length; i++) {
      System.out.print(grades[i]+", ");
    }
    sop("");
    sop("Please insert an integer to be searched for");
    b = scan.nextInt();
    if (linSearch(b, grades)==-1) {
      sop("Not found");
    } else {
      sop("Found after "+linSearch(b,grades)+" iterations.");
    }
  }
}