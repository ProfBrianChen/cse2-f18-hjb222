import java.util.Scanner;
public class RemoveElements{
  public static int[] randomInput() {
    int[] a = new int[10];
    for (int i=0; i<a.length; i++) {
      int c = (int)(Math.random()*10);
      a[i]=c;
    }
    return a;
  }
  public static void sop(String s) {
    System.out.println(s);
  }
  public static int[] delete(int[] list, int pos) {
    Scanner scan = new Scanner(System.in);
    // ensure the index is properly within range
    while (pos < 0 || pos >= list.length) {
      sop("ERR: incorrect index");
      pos = scan.nextInt();
    }
    int[] n = new int[list.length-1];
    // split the copying in two, before and after removed element
    for (int i=0; i<pos; i++) {
      n[i]=list[i];
    }
    for (int i=pos; i<n.length; i++) {
      n[i]=list[i+1];
    }
    return n;
  }
  public static int linSearch(int n, int[] a) {
    for (int i = 0; i < a.length; i++) {
      if (a[i]==n) {
        return i+1;
      }
    }
    return -1;
  }
  public static int[] remove(int[] list, int target) {
    int iterations = 0;
    // linsearch is useful for deleting all instances
    while (linSearch(target, list) != -1) {
      list = delete(list, linSearch(target, list)-1);
      iterations++;
    }
    if (iterations == 0) {
      sop("Element "+target+" was not found.");
    }
    return list;
  }
  public static void main(String [] arg){
	Scanner scan=new Scanner(System.in);
int num[]=new int[10];
int newArray1[];
int newArray2[];
int index,target;
	String answer="";
	do{
  	System.out.print("Random input 10 ints [0-9]");
  	num = randomInput();
  	String out = "The original array is:";
  	out += listArray(num);
  	System.out.println(out);
 
  	System.out.print("Enter the index ");
  	index = scan.nextInt();
  	newArray1 = delete(num,index);
  	String out1="The output array is ";
  	out1+=listArray(newArray1); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out1);
 
      System.out.print("Enter the target value ");
  	target = scan.nextInt();
  	newArray2 = remove(num,target);
  	String out2="The output array is ";
  	out2+=listArray(newArray2); //return a string of the form "{2, 3, -9}"  
  	System.out.println(out2);
  	 
  	System.out.print("Go again? Enter 'y' or 'Y', anything else to quit-");
  	answer=scan.next();
	}while(answer.equals("Y") || answer.equals("y"));
  }
 
  public static String listArray(int num[]){
	String out="{";
	for(int j=0;j<num.length;j++){
  	if(j>0){
    	out+=", ";
  	}
  	out+=num[j];
	}
	out+="} ";
	return out;
  }
}
