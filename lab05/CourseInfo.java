import java.util.Scanner;

public class CourseInfo {
  public static void sop(String msg) {
    System.out.println(msg);
  }
  
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    boolean debounce = false;
    
    sop("Insert the department name");
    debounce = scan.hasNext();
    
     while (!debounce) {
      scan.next();
      sop("Insert a department name (Strings only)");
      debounce = scan.hasNext();
    }
    
    String departmentName = scan.next();
    
    sop("Insert a course number");
    debounce = scan.hasNextInt();
    
    while (!debounce) {
      scan.next();
      sop("Insert a course number (Integers only)");
      debounce = scan.hasNextInt();
    }
    int courseNumber = scan.nextInt();
    
    sop("Insert the time this class starts");
    debounce = scan.hasNext();
    
     while (!debounce) {
      scan.next();
      sop("Insert the time this class starts (Strings only in format hh:mm)");
      debounce = scan.hasNext();
    }
    
    String startHour = scan.next();
    
    sop("Insert the amount of times this class meets per week");
    debounce = scan.hasNextInt();
    
    while (!debounce) {
      scan.next();
      sop("Insert the amount of times this class meets per week (Integers only)");
      debounce = scan.hasNextInt();
    }
    int meetTimes = scan.nextInt();
    
    sop("Insert the name of the professor");
    debounce = scan.hasNext();
    
     while (!debounce) {
      scan.next();
      sop("Insert the name of the professor (Strings only in format)");
      debounce = scan.hasNext();
    }
    
    String professor = scan.next();
    
    sop("Insert the amount of students in this class");
    debounce = scan.hasNextInt();
    
    while (!debounce) {
      scan.next();
      sop("Insert the amount of students in this class (Integers only)");
      debounce = scan.hasNextInt();
    }
    int numStudents = scan.nextInt();
    
    sop("Course "+departmentName+" "+courseNumber+" with "+numStudents+" students is taught by "+professor+" at "+startHour+", "+meetTimes+" times per week.");
    
    
  }
}