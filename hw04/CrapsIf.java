import java.util.Scanner;
/**
* CrapsIf simulates the game of craps, code written using if statements to evaluate the "slang" outcome
*/
public class CrapsIf {
  static int die1; //outcome of die 1
  static int die2; //outcome of die 2
  
  // simple method to avoid having to rewrite "System.out.println" over and over
  public static void sop(String msg) {
    System.out.println(msg);
  }
  
  // method to return a boolean for what die 1 and die 2 equal
  public static boolean dieEq(int d1, int d2) {
    return (die1 == d1 && die2 == d2);
  }
  
  //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    boolean manual = false;
    sop("Please insert a 0 if you'd like to manually set the dice, if not, insert any other number");
    manual = (scan.nextInt() == 0);
    if (manual) {
      sop("Please insert the value of the first die");
      die1 = scan.nextInt();
      sop("Please insert the value of the second die");
      die2 = scan.nextInt();
    } else {
      die1 = (int) (Math.random()*7);
      die2 = (int) (Math.random()*7);
    }
    // assure that the dice are in the correct range
    if (die1 > 6) {
      die1 = 6;
    } else if (die1 < 1) {
      die1 = 1;
    }
    if (die2 > 6) {
      die2 = 6;
    } else if (die2 < 1) {
      die2 = 1;
    }
    
    if (dieEq(1, 1)) {
      sop("Snake Eyes");
    } else if (dieEq(2,1) || dieEq(1,2)) {
      sop("Ace Deuce");
    } else if (dieEq(3,1) || dieEq(1,3)) {
      sop("Easy Four");
    } else if (dieEq(4,1) || dieEq(1,4) || dieEq(3,2) || dieEq(2,3)) {
      sop("Fever Five");
    } else if (dieEq(5,1) || dieEq(1,5) || dieEq(4,2) || dieEq(2,4)) {
      sop("Easy Six");
    } else if (dieEq(6,1) || dieEq(1,6) || dieEq(5,2) || dieEq(2,5) || dieEq(4,3) || dieEq(3,4)) {
      sop("Seven Out");
    } else if (dieEq(2,2)) {
      sop("Hard Four");
    } else if (dieEq(6,2) || dieEq(2,6) || dieEq(5,3) || dieEq(3,5)) {
      sop("Easy Eight");
    } else if (dieEq(3,3)) {
      sop("Hard Six");
    } else if (dieEq(6,3) || dieEq(3,6) || dieEq(5,4) || dieEq(4,5)) {
      sop("Nine");
    } else if (dieEq(4,4)) {
      sop("Hard Eight");
    } else if (dieEq(6,4) || dieEq(4,6)) {
      sop("Easy Ten");
    } else if (dieEq(5,5)) {
      sop("Hard Ten");
    } else if (dieEq(6,5) || dieEq(5,6)) {
      sop("Yo-leven");
    } else if (dieEq(6,6)) {
      sop("Boxcars");
    }
  }// end main method
}// end class