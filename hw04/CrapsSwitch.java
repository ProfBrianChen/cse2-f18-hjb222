import java.util.Scanner;
/**
* CrapsIf simulates the game of craps, code written a switch statement to evaluate the "slang" outcome
*/
public class CrapsSwitch {
  static int die1; //outcome of die 1
  static int die2; //outcome of die 2
  
  // simple method to avoid having to rewrite "System.out.println" over and over
  public static void sop(String msg) {
    System.out.println(msg);
  }
  
  //main method
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    boolean manual = false;
    sop("Please insert a 0 if you'd like to manually set the dice, if not, insert any other number");
    manual = (scan.nextInt() == 0);
    if (manual) {
      sop("Please insert the value of the first die");
      die1 = scan.nextInt();
      sop("Please insert the value of the second die");
      die2 = scan.nextInt();
    } else {
      die1 = (int) (Math.random()*7);
      die2 = (int) (Math.random()*7);
    }
    // assure that the dice are in the correct range
    if (die1 > 6) {
      die1 = 6;
    } else if (die1 < 1) {
      die1 = 1;
    }
    if (die2 > 6) {
      die2 = 6;
    } else if (die2 < 1) {
      die2 = 1;
    }
    
    switch (die1) {
      case 1:
        switch (die2) {
          case 1:
            sop("Snake Eyes");
            break;
          case 2:
            sop("Ace Deuce");
            break;
          case 3:
            sop("Easy Four");
            break;
          case 4:
            sop("Fever Five");
            break;
          case 5:
            sop("Easy Six");
            break;
          case 6:
            sop("Seven Out");
            break;
        }
        break;
      case 2:
        switch (die2) {
          case 1:
            sop("Ace Deuce");
            break;
          case 2:
            sop("Hard Four");
            break;
          case 3:
            sop("Fever Five");
            break;
          case 4: 
            sop("Easy Six");
            break;
          case 5:
            sop("Seven Out");
            break;
          case 6:
            sop("Easy Eight");
            break;
        }
        break;
      case 3:
        switch (die2) {
          case 1:
            sop("Easy Four");
            break;
          case 2:
            sop("Fever Five");
            break;
          case 3:
            sop("Hard Six");
            break;
          case 4: 
            sop("Seven Out");
            break;
          case 5:
            sop("Easy Eight");
            break;
          case 6:
            sop("Nine");
            break;
        }
        break;
      case 4:
        switch (die2) {
          case 1:
            sop("Fever Five");
            break;
          case 2:
            sop("Easy Six");
            break;
          case 3:
            sop("Seven Out");
            break;
          case 4: 
            sop("Hard Eight");
            break;
          case 5:
            sop("Nine");
            break;
          case 6:
            sop("Easy Ten");
            break;
        }
        break;
      case 5:
        switch (die2) {
          case 1:
            sop("Easy Six");
            break;
          case 2:
            sop("Seven Out");
            break;
          case 3:
            sop("Easy Eight");
            break;
          case 4: 
            sop("Nine");
            break;
          case 5:
            sop("Hard Ten");
            break;
          case 6:
            sop("Yo-leven");
            break;
        }    
        break;
      case 6:
        switch (die2) {
          case 1:
            sop("Seven Out");
            break;
          case 2:
            sop("Easy Eight");
            break;
          case 3:
            sop("Nine");
            break;
          case 4: 
            sop("Easy Ten");
            break;
          case 5:
            sop("Yo-leven");
            break;
          case 6:
            sop("Boxcars");
            break;
        }    
        break;
    }
  } // end main method
}// end CrapsSwitch