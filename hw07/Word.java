import java.util.Scanner;

public class Word {
  public static void sop(String s) {
    System.out.println(s);
  }
  public static String sampleText(String s) {
   System.out.println("You entered: "+s);
    return s;
  }
  public static int getNumOfNonWSCharacters(String s) {
    int x = 0;
    for (int i = 0; i<s.length(); i++) {
      if (s.charAt(i)!=' ') {
        x++;
      }
    }
    return x;
  }
  public static int getNumOfWords(String s) {
    int x = 1;
    for (int i = 0; i<s.length(); i++) {
      if (s.charAt(i)==' ') {
        x++;
      }
    }
    return x;
  }
  public static String replaceExclamation(String s) {
    s=s.replaceAll("!", ".");
    return s;
  }
  public static int findText(String s, String j) {
    int tot = 0;
    int jump = 0;
    for (int i = 0; i<s.length(); i++) {
      if (s.indexOf(j, i) != -1) {
        jump = s.indexOf(j,i);
        i+=jump;
        tot++;
      }
    }
    return tot;
  }
  public static String shortenSpace(String s) {
    boolean a = true;
    for (int i=s.length()-1; i>0; i--) {
      if (s.charAt(i) == ' ') {
       if (i!=s.length()-1 && s.charAt(i+1)==' ') {
         s = s.substring(0, i) + s.substring(i+1);
       }
      }
    }
    return s;
  }
  public static void printMenu(String toChange) {
    Scanner scan = new Scanner(System.in);
    sop("MENU");
    sop("c - number of non-whitespace characters");
    sop("w - number of words");
    sop("f - find text");
    sop("r - replace all !'s");
    sop("s - shorten spaces");
    sop("q - quit");
    
    String s = scan.nextLine();
     switch(s) {
       case "c": 
         sampleText("c");
         sop("Num of non-whitespace: "+getNumOfNonWSCharacters(toChange));
         break;
       case "w":
         sampleText("w");
         sop("Num of words: "+getNumOfWords(toChange));
         break;
       case "f":
         sampleText("f");
         sop("Enter a word or phrase to be found:");
         String v = scan.nextLine();
         sop("'"+v+"'"+" instances: "+findText(toChange, v));
         break;
       case "r":
         sampleText("r");
         sop(replaceExclamation(toChange));
         break;
       case "s":
         sampleText("s");
         sop(shortenSpace(toChange));
         break;
       case "q":
         sampleText("q");
         break;
       default: 
         sampleText("ERR: incorrect input");
         printMenu(toChange);
         break;
         
     }
  }
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Please insert a message");
    String s = scan.nextLine();
    sampleText(s);
    printMenu(s);
    
  }
}