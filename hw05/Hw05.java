// Harry Boon
// 10/9/2018
import java.util.Scanner;

public class Hw05 {
  public static void main(String[] args) {
    Scanner scan = new Scanner(System.in);
    System.out.println("Please insert an integer amount of hands to be generated (any number > 0)");
    int n = scan.nextInt();
    double fourOfAKind = 0;
    double threeOfAKind = 0;
    double twoPair = 0;
    double onePair = 0;
    int i = 0;
    
    // first loop runs to generate the amount of hands that the user inputs
    while (i<n) {
      int[] hand = {0,0,0,0,0};
      // following ints to track one pair, two of a kind, etc.
      int one=0;
      int two=0;
      int three=0;
      int four=0;
      
      int x = 0;
      
      // setting the hand
      while (x < 5) {
        int card = 1+((int) (Math.random()*52));
        hand[x] = card;
        for (int j = x-1; j>0; j--) {
          if (hand[j]==hand[x]) {
            hand[x] = 0;
            x--; // set the loop back one so it re-trys that card (no repeats)
            break;
          }
        }
        x++;
      }
      
      // nested loop to compare items in the hand array with each other
      for (int f = 0; f<5; f++) {
        int matching = 0; // used to track how many cards hand[j] matches with
        for (int g = f+1; g<5; g++) {
          if (Math.abs(hand[f]-hand[g])%13==0) {
            matching++;
          }
        }
        // if there is only one match on hand[j] BUT, there was a match before (i.e: one pair was already found),
        // then the hand MUST be a two pair
        if (matching == 1 && one > 0) {
          two += 10;
        } else if (matching == 1 && one == 0) {
          one++;
        }
        // if hand[j] matches two other cards, then it's three of a kind
        if (matching == 2) {
          three+=50;
        }
        // if hand[j] matches three other cards, then it's four of a kind
        if (matching == 3) {
          four+=100;
        }
      }
      // simple point system to extract what kind of hand it is
      int b = Math.max(Math.max(one, two), Math.max(three, four));
      if (b > 0) { // if b == 0 then there was no match whatsoever
        if (b == one) {
          onePair++;
        } else if (b == two) {
          twoPair++;
        } else if (b == three) {
          threeOfAKind++;
        } else if (b == four) {
          fourOfAKind++;
        }
      }
      i++;
    }
    System.out.println("The number of loops: "+n);
    System.out.println("The probability of four of a kind: "+(fourOfAKind/n));
    System.out.println("The probability of three of a kind: "+(threeOfAKind/n));
    System.out.println("The probability of two pairs: "+(twoPair/n));
    System.out.println("The probability of one pair: "+(onePair/n));
  }
}