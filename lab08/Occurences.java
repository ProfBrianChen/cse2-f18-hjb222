public class Occurences {
  public static void main(String[] args) {
    int[] arr1 = new int[100];
    int[] arr2 = new int[100];
    
    for (int i = 0; i<100; i++) {
      int rNum = (int)(Math.random()*100);
      arr1[i] = rNum;
      arr2[rNum] += 1;
      System.out.print(rNum+", ");
    }
    System.out.println();
    for (int m = 0; m<100; m++){
      System.out.println(m+" occurs "+arr2[m]+" times.");
    }
  }
}