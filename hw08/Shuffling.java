// Harry Boon
// 11/13/2018
import java.util.Scanner;

public class Shuffling{ 
  
  public static void printArray(String[] list) {
    for (String v: list) {
      System.out.print(v+" ");
    }
    System.out.println();
  }
  
  public static String[] shuffle(String[] list) {
    System.out.println("Shuffled");
    for (int i=0; i<60; i++) {
      int r = 1+((int)(Math.random()*51));
      String c = list[0]; // must save a copy to swap the values
      list[0]=list[r];
      list[r]=c;
    }
    
    return list;
  }
  public static String[] getHand(String[] list, int index, int numCards) {
    System.out.println("Hand");
    String[] hand = new String[5];
    int n = 0; // this index will be used to transfer the cards over to the hand
    for (int i=index; i>index-5; i--) {
      hand[n]=list[i];
      n++;
    }
    return hand;
  }
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
     //suits club, heart, spade or diamond 
     String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 5; 
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
    } 
    System.out.println();
    printArray(cards); 
    // had to edit this line below as java does not pass by reference and the methods can't access the variables in the scope of the main method
    cards = shuffle(cards); 
    printArray(cards); 
    while(again == 1){ 
       hand = getHand(cards,index,numCards); 
       printArray(hand);
       index = index - numCards;
      if (numCards > index + 1) { // if statement to satisfy the condition in the homework when the index is too low for numCards, and we need a new deck
     	index = 51;
        cards = shuffle(cards);
      }
       System.out.println("Enter a 1 if you want another hand drawn"); 
       again = scan.nextInt(); 

    }  
  } 
}
