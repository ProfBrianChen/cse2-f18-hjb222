import java.util.Scanner;
/**
* Used for calculating the price that one person at dinner should pay when the bill and tax is split amongts all party members
*/
public class Check {
  //main method
  public static void main(String[] args) {
    // new scanner object
    Scanner myScanner = new Scanner(System.in);
    // request cost of original check
    System.out.print("Enter the original cost of the check in the form xx.xx: ");
    double checkCost = myScanner.nextDouble();
    // request the tip percent
    System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx): ");
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //We want to convert the percentage into a decimal value
    // request the number of people that went to dinner
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt();
    
    double totalCost;
    double costPerPerson;
    int dollars, dimes, pennies;  //whole dollar amount of cost, dimes and pennies for storing digits
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    // whole dollar amount, using integer casting
    dollars=(int)costPerPerson;
    // dimes multiplied by 10 because 100/10=10, pennies because 100/1=100
    // mod 10 to get the integer total of pennies or dimes
    dimes=(int)(costPerPerson * 10) % 10;
    pennies=(int)(costPerPerson * 100) % 10;
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);

  }// end main method
}// end class