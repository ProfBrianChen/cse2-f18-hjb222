/** 
* Card generator picks a random card from a deck
*/
public class CardGenerator {
  //main method
  public static void main(String[] args) {
    String[] suits = {"Clubs", "Hearts", "Spades", "Diamonds"};
    String[] cards = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
    
    int rSuit = (int) (Math.random()*4);
    int rCard = (int) (Math.random()*13);
    
    System.out.println("You picked the "+cards[rCard]+" of "+suits[rSuit]);
  }// end main method
}// end class