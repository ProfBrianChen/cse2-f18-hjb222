/**
* Cyclometer tracks data about a bike trip. It should: 
* print the number of minutes for each trip.
* Print the number of counts for each trip.
* Print the distance of each trip in miles.
* Print the distance for the two trips combined.
*/

public class Cyclometer {

  
  // main method
  public static void main(String[] args) {
    int secsTrip1=480;  // the amount of seconds in the first trip
    int secsTrip2=3220;  // the amount of seconds in the second trip
    int countsTrip1=1561;  // the number of counts in the first trip
    int countsTrip2=9037; // the number of counts in the second trip 
    double wheelDiameter=27.0;  // diameter of the bike wheel
  	double pi = Math.PI; // pi, useful for circular calculations and stuff i.e: bike wheels
  	int feetPerMile=5280;  // conversion of the amount of feet in a mile
  	int inchesPerFoot=12;   // conversion for the amount of inches in a foot
  	int secondsPerMinute=60;  // conversion for the amount of seconds in a minute
	  double distanceTrip1, distanceTrip2, totalDistance;  // these represent the distance the bike traveled, will be assigned values later
    
    System.out.println("Trip 1 took "+
     (secsTrip1/secondsPerMinute)+" minutes and had "+
     countsTrip1+" counts.");
    System.out.println("Trip 2 took "+
     (secsTrip2/secondsPerMinute)+" minutes and had "+
     countsTrip2+" counts.");
    
    // the calculations below are to determine the distance of each trip, and the combined total distance
    
    // simple calculation that multiplys the circumference of the wheel by the number of wheel turns for distance
    distanceTrip1=countsTrip1*wheelDiameter*pi;
    // converting the aforementioned distance into miles
    distanceTrip1/=inchesPerFoot*feetPerMile;
    // repeating the previous 2 calculations but for the second trip
    distanceTrip2=countsTrip2*wheelDiameter*pi/inchesPerFoot/feetPerMile;
    // adding both trips for total distance in miles
    totalDistance=distanceTrip1+distanceTrip2;   
    
    // displaying distance information about the trip to the user
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");

  } // end of main method
} // end of class