/**
* Arithmetic is a class used to calculate sales tax on items of clothing
*/
public class Arithmetic {
  
  /**
  * A method to force a double with many decimal places to have only two figures following the decimal
  * @param c: the double to have only 2 decimal places
  * @return the edited double with 2 decimal places
  */
  public static double twoDec(double c) {
      c = 100 * c;
      c = (int) c;
      c /= 100.0;
      return c;
    }
  
  //main method
  public static void main(String[] args) {
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;

    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt
    double shirtPrice = 24.99;

    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;

    //the tax rate
    double paSalesTax = 0.06;
    
    // below are calculations for the total cost of each item bought at their respective quantities
    double totalPantsCost = twoDec(numPants * pantsPrice);
    double totalShirtCost = twoDec(numShirts * shirtPrice);
    double totalBeltCost = twoDec(numBelts * beltCost);
    
    // below calculates the total tax for the articles of clothing
    double pantTax = twoDec(totalPantsCost*paSalesTax);
    double shirtTax = twoDec(totalShirtCost * paSalesTax);
    double beltTax = twoDec(totalBeltCost * paSalesTax);
    
    // below is the total cost of all items before tax
    double itemsCost = twoDec(totalPantsCost+totalShirtCost+totalBeltCost);
    
    // below gives the total tax added to the items of clothing, and the overall price of all items with tax
    double totalTax = twoDec(pantTax + shirtTax + beltTax);
    double totalPrice = twoDec(itemsCost + totalTax);
    
    // print statements
    System.out.println("total pants cost "+totalPantsCost);
    System.out.println("total shirt cost "+totalShirtCost);
    System.out.println("total belt cost "+totalBeltCost);
    System.out.println("pant tax "+pantTax);
    System.out.println("shirt tax "+shirtTax);
    System.out.println("belt tax "+beltTax);
    System.out.println("items cost without tax "+itemsCost);
    System.out.println("total tax for items "+totalTax);
    System.out.println("total price with tax "+totalPrice);
    

  }// end main method
}// end Arithmethic